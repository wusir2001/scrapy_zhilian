# coding:utf-8
# 配置文件
import os
import redis

basedir = os.path.abspath(os.path.dirname(__file__))


# 基类
class Config:
    SECRET_KEY = 'tanxiangyu'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    FLASKY_MAIL_SUBJECT_PREFIX = '[Flask]'
    FLASKY_MAIL_SENDER = 'Flask Admin'
    FLASKY_ADMIN = 'fanfzj'
    CACHE_TYPE = 'redis'

    CACHE_REDIS_PORT = 6379
    CACHE_REDIS_DB = '2'
    #CACHE_REDIS_PASSWORD = 'FanTan879425'
    # 邮箱
    MAIL_SERVER = 'smtp.exmail.qq.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = "fanzhijun@ibeifeng.com"
    MAIL_PASSWORD = "FanTan879"
    MAIL_DEFAULT_SENDER = "fanzhijun@ibeifeng.com"

    @staticmethod
    def init_app(app):
        pass


# 开发环境
class DevelopmentConfig(Config):
    DEBUG = True
    MAIL_DEBUG = DEBUG
    CACHE_REDIS_HOST = '172.16.101.128'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@localhost/zp?charset=utf8'
    SESSION_REDIS = redis.Redis(host=CACHE_REDIS_HOST, port=Config.CACHE_REDIS_PORT,db=Config.CACHE_REDIS_DB)


# 测试环境
class TestingConfig(Config):
    DEBUG = True
    MAIL_DEBUG = DEBUG
    CACHE_REDIS_HOST = '172.16.101.128'
    SESSION_REDIS = redis.Redis(host=CACHE_REDIS_HOST, port=Config.CACHE_REDIS_PORT, db=Config.CACHE_REDIS_DB)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'zp.sqlite')


# 生产环境
class ProductionConfig(Config):
    DEBUG = False
    MAIL_DEBUG = DEBUG
    CACHE_REDIS_HOST = '127.0.0.1'
    SESSION_REDIS = redis.Redis(host=CACHE_REDIS_HOST, port=Config.CACHE_REDIS_PORT,db=Config.CACHE_REDIS_DB)
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@localhost/zp?charset=utf8'


# 设置一个config字典，注册不同的配置环境
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}