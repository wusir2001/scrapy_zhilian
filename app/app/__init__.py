# -*- coding:utf-8 -*-
from app.config import config
from flask import Flask
from flask_cache import Cache
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session

mail = Mail()
db = SQLAlchemy()
cache = Cache()
session=Session()

# 工厂函数
def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    mail.init_app(app)
    db.init_app(app)
    cache.init_app(app, config)
    app.config['SESSION_TYPE'] = 'sqlalchemy'  # session类型为sqlalchemy
    app.config['SESSION_SQLALCHEMY'] = db  # SQLAlchemy对象
    app.config['SESSION_SQLALCHEMY_TABLE'] = 'ibf_session_log'  # session要保存的表名称
    app.config['SESSION_PERMANENT'] = True  # 如果设置为True，则关闭浏览器session就失效。
    app.config['SESSION_USE_SIGNER'] = False  # 是否对发送到浏览器上session的cookie值进行加密
    app.config['SESSION_KEY_PREFIX'] = 'session:'  # 保存到session中的值的前缀
    session.init_app(app)

    # 注册蓝本
    from app.main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app

